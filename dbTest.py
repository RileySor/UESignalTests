import os
import sys
import time
import subprocess
import shlex
from timeit import default_timer as timer

subprocess.call(["pnadb", "-a"])
os.system("adb shell input keyevent 26")

results = open("Results.txt", "w")
results.write(str("RESULTS: \n \n"))

ATTENPATH = input("What is the attenuator path? ")
ATTEN = 0
START = timer()
while (ATTEN <= 95):
	comandLine = "/users/gtw/atten -r " + str(ATTENPATH) + " " + str(ATTEN)
	args = shlex.split(comandLine)
	subprocess.call(args)
	x = 0
	while (x <= 3):
		DB = os.popen("adb shell dumpsys telephony.registry | grep -i signalstrength").read()
		CONN = os.popen("adb shell dumpsys telephony.registry | grep -i mdataconnectionstate").read()
		TIME = timer()
		TIME = TIME - START
		results.write(str("TIME: \n"))
		results.write(str(TIME))
		results.write(str("\n ATTENUATION: \n"))
		results.write(str(ATTEN))
		results.write(str("\n SIGNAL STRENGTH: \n"))
		results.write(str(DB))
		results.write(str("CONNECTION STATE: \n"))
		results.write(str(CONN))
		results.write(str("\n \n"))
		x += 1
		time.sleep(.25)
		
	ATTEN += 1

print "Test results are in Results.txt"

