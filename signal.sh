#! /bin/bash

#connect phone to adb target
pnadb -a

#test if phone is connected to LTE
#if not exits script
if [ "$(adb shell dumpsys telephony.registry | grep -i mdataconnectionstate | cut -c24)" != "2" ]; then
	echo " CONNECTION STATE: $(adb shell dumpsys telephony.registry | grep -i mdataconnectionstate | cut -c24-25)"
	echo "Phone not connected to an LTE network"
	echo "Reboot UE and eNB"
	exit 0
fi

#get amout of time the script will run
echo "Enter how many seconds you would like to test for signal:"
read TIME

#makes an ifinite loop when TIME = i
if [ $TIME == "i" ]; then
	while [ true ]; do
		adb shell dumpsys telephony.registry | grep -i signalstrength | cut -c63-67
        	CONNECTION="$(adb shell dumpsys telephony.registry | grep -i mdataconnectionstate | cut -c24)"
		if [ "$CONNECTION" != "2" ]
		then echo "NOT CONNECTED TO LTE"
		else echo "CONNECTED"
		fi
		sleep 0.25s
	done
fi

#sends signal stregnth until COUNTER = TIME
COUNTER=0
while [ $COUNTER -lt $TIME ]; do
        adb shell dumpsys telephony.registry | grep -i signalstrength | cut -c63-67
        CONNECTION="$(adb shell dumpsys telephony.registry | grep -i mdataconnectionstate | cut -c24)"
		if [ "$CONNECTION" != "2" ]
		then echo "NOT CONNECTED TO LTE"
		else echo "CONNECTED"
		fi
        ((COUNTER++))
        sleep 0.5s
	adb shell dumpsys telephony.registry | grep -i signalstrength | cut -c63-67
        CONNECTION="$(adb shell dumpsys telephony.registry | grep -i mdataconnectionstate | cut -c24)"
		if [ "$CONNECTION" != "2" ]
		then echo "NOT CONNECTED TO LTE"
		else echo "CONNECTED"
		fi
	sleep 0.5s
done

